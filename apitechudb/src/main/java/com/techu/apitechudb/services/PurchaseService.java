package com.techu.apitechudb.services;

import com.techu.apitechudb.models.PurchaseModel;
import com.techu.apitechudb.repositories.UserRepository;
import com.techu.apitechudb.repositories.ProductRepository;
import com.techu.apitechudb.repositories.PurchaseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class PurchaseService {

    @Autowired
    PurchaseRepository purchaseRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    ProductRepository productRepository;


    public List<PurchaseModel> getPurchases(){
        System.out.println("getPurchases");

        return this.purchaseRepository.findAll();
    }

    public PurchaseServiceResponse addPurchase(PurchaseModel purchase) {

        System.out.println("addPurchase");

        PurchaseServiceResponse result = new PurchaseServiceResponse();

        result.setPurchase(purchase);

        if (this.userRepository.findById(purchase.getUserId()).isEmpty() == true) {
            System.out.println("El usuario de la compra NO se ha encontrado");

            result.setMsg("El usuario de la compra NO se ha encontrado");
            result.setResponseHttpStatusCode(HttpStatus.BAD_REQUEST);

            return result;
        }

        if (this.purchaseRepository.findById(purchase.getId()).isPresent() == true) {
            System.out.println("Ya hay una compra con ese ID");

            result.setMsg("Ya hay una compra con ese ID");
            result.setResponseHttpStatusCode(HttpStatus.BAD_REQUEST);

            return result;
        }

        float amount = 0;
        for (Map.Entry<String, Integer> purchaseItem : purchase.getPurchaseItems().entrySet()) {
            if (this.productRepository.findById(purchaseItem.getKey()).isEmpty()) {
                System.out.println("El producto con la id " + purchaseItem.getKey() + "no encontrado");

                result.setMsg("El producto con la id " + purchaseItem.getKey() + "no encontrado");
                result.setResponseHttpStatusCode(HttpStatus.BAD_REQUEST);

                return result;
            } else {
                System.out.println("Añadiendo valor de " + purchaseItem.getValue() + "unidades de producto al total");

                amount +=
                        (this.productRepository.findById(purchaseItem.getKey()).get().getPrice() * purchaseItem.getValue());
            }
        }

        purchase.setAmount(amount);

        this.purchaseRepository.save(purchase);
        result.setMsg("Compra añadida correctamente");
        result.setResponseHttpStatusCode(HttpStatus.CREATED);

        return result;

    }


}

