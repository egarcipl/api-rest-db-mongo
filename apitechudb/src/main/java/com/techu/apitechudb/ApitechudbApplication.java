package com.techu.apitechudb;

import com.techu.apitechudb.models.UserModel;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.ArrayList;

@SpringBootApplication
public class ApitechudbApplication {

	public static ArrayList<UserModel> userModels;

	public static void main(String[] args) {

		SpringApplication.run(ApitechudbApplication.class, args);
	}

}
