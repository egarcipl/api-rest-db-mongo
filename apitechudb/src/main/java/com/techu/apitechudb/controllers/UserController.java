package com.techu.apitechudb.controllers;

import com.techu.apitechudb.ApitechudbApplication;
import com.techu.apitechudb.models.UserModel;
import com.techu.apitechudb.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.data.web.SpringDataWebProperties;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;


//Al poner el requestMapping delante del controlador se añade este prefijo a todas las invocaciones
//por url
@RestController
@RequestMapping("/apitechu/v3")
public class UserController {

    @Autowired
    UserService userService;

    // El $ es una convención http
    @GetMapping("/users")
    public ResponseEntity<List<UserModel>> getUsers(
            @RequestParam (name = "$orderBy", required = false) String orderby)
    {
        System.out.println("getUsers");
        System.out.println("order by es: "+ orderby);

        return new ResponseEntity<>(this.userService.getUsers(orderby), HttpStatus.OK);
    }

    @PostMapping("/users")
    public ResponseEntity<UserModel> addUser(@RequestBody UserModel user) {
        System.out.println("addUser");
        System.out.println("La id del usuario que se va a crear es " + user.getId());
        System.out.println("El nombre del usuario que se va a crear es " + user.getName());
        System.out.println("La edad del usuario que se va a crear es " + user.getAge());

        return new ResponseEntity<>(this.userService.add(user), HttpStatus.CREATED);
        //return new UserModel();
    }


    @GetMapping("users/{id}")
    public ResponseEntity<Object> getUserById(@PathVariable String id){
        System.out.println("getUserById");
        System.out.println("La id del usuario a buscar es " + id);

        // Declaración e inicialización de variable
        Optional<UserModel> result = this.userService.findById(id);

        // primero vemos si hay dato (isPresent) , si esta presente devolvemos el get del objeto y
        // si no, un mensaje. Esta forma de poner el if se llama operador ternario
        // (condicion ? valor devuelto si true : valor devuelto si false)
        return new ResponseEntity<>(
                result.isPresent() ? result.get() : "Usuario no encontrado",
                result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
        //  return new ResponseEntity<>( new UserModel(), HttpStatus.OK);

    }

    @PutMapping("/users/{id}")
    public ResponseEntity<UserModel> updateUser(@RequestBody UserModel userModel, @PathVariable String id){
        System.out.println("updateUser");
        System.out.println("La id del usuario que se va a actualizar en parametro es "+id);
        System.out.println("El nombre del usuario que se va a actualizar es "+ userModel.getName());
        System.out.println("La edad del usuario que se va a actualizar es "+ userModel.getAge());

        //Ojo, nuestro update que tenemos en nuestros servicios hace un save, es decir, que si no existe,lo insertaría, es
        // por esto por lo que tenemos que meter logica de negocio en el controlador (la logica que hemos añadido
        // es mas propia del servicio, no es de negocio sino mas pegada a la BBDD, pero lo ponemos como ejemplo
        // para ilustrar que la logica de necogio deberia ir aqui

        Optional <UserModel> userToUpdate = this.userService.findById(id);

        if(userToUpdate.isPresent()){
            System.out.println("Usuario para actualizar encontrado, actualizando");
            this.userService.update(userModel);
        } else{
            System.out.println("Usuario para actualizar NO encontrado");
        }


        return new ResponseEntity<>(userModel,
                userToUpdate.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND);

        //        return new ResponseEntity<>(new UserModel(), HttpStatus.OK);
    }

    @DeleteMapping("/users/{id}")
    public ResponseEntity<String> deleteUser(@PathVariable String id){
        System.out.println("deleteUser");
        System.out.println("La id del usuario a borrar es "+ id);

        boolean deletedUser= this.userService.delete(id);

        return new ResponseEntity<>(
                deletedUser ? "Usuario borrado" : "Usuario no borrado",
                deletedUser ? HttpStatus.OK :  HttpStatus.NOT_FOUND
        );
    }

}
