package com.techu.apitechudb.controllers;

import com.techu.apitechudb.models.ProductModel;
import com.techu.apitechudb.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

//Al poner el requestMapping delante del controlador se añade este prefijo a todas las invocaciones
//por url
@RestController
@RequestMapping("/apitechu/v2")
public class ProductController {

    @Autowired
    ProductService productService;

    @GetMapping("/products")
    public ResponseEntity<List<ProductModel>> getProducts(){
        System.out.println("getProducts");

        return new ResponseEntity<>(this.productService.findAll(), HttpStatus.OK);

    }

    @PostMapping("/products")
    public ResponseEntity<ProductModel> addProduct(@RequestBody ProductModel product){
        System.out.println("addProduct");
        System.out.println("La id del producto que se va a crear es " + product.getId());
        System.out.println("La descripcion del producto que se va a crear es " + product.getDesc());
        System.out.println("El precio del producto que se va a crear es " + product.getPrice());

        return new ResponseEntity<>(this.productService.add(product), HttpStatus.CREATED);
        //return new ProductModel();
    }

    // TRUCO: la función devolvía inicialmente un ProductModel, pero al incorporar la parte de
    // result.isPresent podría ocurrir que devuelva el string "Producto no encontrado", por lo
    // que java da error pk esto no es un objeto tipo ProductModel, Como truco se debe cambiar
    // por devolver un producto generico tipo <Object>. Tambien podria devolverse un <?>, es
    // raro verlo asi pero Java lo permite
    @GetMapping("products/{id}")
    public ResponseEntity<Object> getProductById(@PathVariable String id){
        System.out.println("getProductById");
        System.out.println("La id del producto a buscar es " + id);

        // Declaración e inicialización de variable
        Optional<ProductModel> result = this.productService.findById(id);

        // primero vemos si hay dato (isPresent) , si esta presente devolvemos el get del objeto y
        // si no, un mensaje. Esta forma de poner el if se llama operador ternario
        // (condicion ? valor devuelto si true : valor devuelto si false)
        return new ResponseEntity<>(
                result.isPresent() ? result.get() : "Producto no encontrado",
                result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
      //  return new ResponseEntity<>( new ProductModel(), HttpStatus.OK);

    }

    @PutMapping("/products/{id}")
    public ResponseEntity<ProductModel> updateProduct(@RequestBody ProductModel productModel, @PathVariable String id){
        System.out.println("updateProduct");
        System.out.println("La id del producto que se va a actualizar en parametro es "+id);
        System.out.println("La id del producto que se va a actualizar es "+ productModel.getId());
        System.out.println("La descripcion del producto que se va a actualizar es "+ productModel.getDesc());
        System.out.println("El precio del producto que se va a actualizar es "+ productModel.getPrice());

        //Ojo, nuestro update que tenemos en nuestros servicios hace un save, es decir, que si no existe,lo insertaría, es
        // por esto por lo que tenemos que meter logica de negocio en el controlador (la logica que hemos añadido
        // es mas propia del servicio, no es de negocio sino mas pegada a la BBDD, pero lo ponemos como ejemplo
        // para ilustrar que la logica de necogio deberia ir aqui

        Optional <ProductModel> productToUpdate = this.productService.findById(id);

        if(productToUpdate.isPresent()){
            System.out.println("Producto para actualizar encontrado, actualizando");
            this.productService.update(productModel);
        }

        return new ResponseEntity<>(productModel,
                productToUpdate.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND);

        //        return new ResponseEntity<>(new ProductModel(), HttpStatus.OK);
    }

    @DeleteMapping("/products/{id}")
    public ResponseEntity<String> deleteProduct(@PathVariable String id){
        System.out.println("deleteProduct");
        System.out.println("La id del producto a borrar es "+ id);

        boolean deletedProduct= this.productService.delete(id);

        return new ResponseEntity<>(
                deletedProduct ? "Producto borrado" : "Producto no borrado",
                deletedProduct ? HttpStatus.OK :  HttpStatus.NOT_FOUND
        );
    }

}
