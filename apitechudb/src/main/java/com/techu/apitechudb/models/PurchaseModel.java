package com.techu.apitechudb.models;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Map;

@Document(collection = "purchases")
public class PurchaseModel {

    // El @Id mapea el campo que tiene justo debajo. Lo usaremos como el identificador de la bbdd
    @Id
    private String id;
    private String userId;
    private float amount;
    private Map<String, Integer> purchaseItems;

    //Constructores de la clase
    public PurchaseModel() {
    }

    public PurchaseModel(String id, String userId, float amount, Map<String, Integer> purchaseItems) {
        this.id = id;
        this.userId = userId;
        this.amount = amount;
        this.purchaseItems = purchaseItems;
    }

    public String getId() {
        return this.id;
    }

    public String getUserId() {
        return this.userId;
    }

    public float getAmount() {
        return this.amount;
    }

    public Map<String, Integer> getPurchaseItems() {
        return this.purchaseItems;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public void setAmount(float amount) {
        this.amount = amount;
    }

    public void setPurchaseItems(Map<String, Integer> purchaseItems) {
        this.purchaseItems = purchaseItems;
    }

}

