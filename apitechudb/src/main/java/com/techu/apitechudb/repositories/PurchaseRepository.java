package com.techu.apitechudb.repositories;

import com.techu.apitechudb.models.ProductModel;
import com.techu.apitechudb.models.PurchaseModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
// Objeto tipo purchase y el string que es la id
public interface PurchaseRepository extends MongoRepository<PurchaseModel, String> {
}

