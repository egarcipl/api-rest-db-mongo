package com.techu.apitechudb.repositories;

import com.techu.apitechudb.models.UserModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

//MongoRepository es una clase ya creada para manejar operaciones sobre mongo de tipo generico
// Esto se va a traer un productmodel en base a un string

@Repository
public interface UserRepository extends MongoRepository <UserModel, String> {
}
